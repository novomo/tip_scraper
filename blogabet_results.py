from time import sleep
import selenium
import os
from dotenv import load_dotenv
import mysqlx
import pytz
from datetime import datetime, timedelta
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")


def blogabet_results(self, task, url=None):
    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['blogabet_results']['driver']
                         ]['driver'].switch_to.window(self.tabs['blogabet_results']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "blogabet_results", "https:/blogabet.com", "standard")

    def click_on_menu(linkText):
        while True:
            try:
                menuBtn = self.drivers[self.tabs['blogabet_results']['driver']
                                       ]['driver'].find_element_by_class_name(
                    'btn-blogmenu')
                self.drivers[self.tabs['blogabet_results']['driver']
                             ]['driver'].execute_script(
                    "arguments[0].scrollIntoView();", menuBtn)
                self.drivers[self.tabs['blogabet_results']['driver']
                             ]['driver'].execute_script(
                    'arguments[0].click()', menuBtn)
            except selenium.common.exceptions.NoSuchElementException:
                return False
            # stop slow interaction with site for bot controls
            sleep(2)
            try:
                menu = self.headlessWaitForElement(
                    "visible", 'blog-menu', self.By.CLASS_NAME)
            except selenium.common.exceptions.TimeoutException:
                continue
            break
        try:
            followingLink = menu.find_element_by_partial_link_text(linkText)
            print(followingLink)
            followingLink.click()
        except selenium.common.exceptions.ElementClickInterceptedException:
            buttons = self.drivers[self.tabs['blogabet_results']['driver']
                                   ]['driver'].find_elements_by_xpath(
                "//*[contains(text(), 'Yes, I am over 18')]")
            buttons[0].click()
            sleep(2)
            followingLink = menu.find_element_by_partial_link_text(linkText)
            print(followingLink)
            followingLink.click()

    def go_to_archive_page():
        click_on_menu("PICKS ARCHIVE")

    def sendTipResult(reultString, url):
        query = f"""
            mutation {{
                updateTip( inputTip: {{
                    url: "{url}",
                    result: "{resultString}"
                }} ) {{
                _id
                }}

            }}
        """
        self.sendToApi(query)

    if "blogabet_results" not in self.tabs:
        print("opening")
        self.openURL(
            "blogabet_results", "https:/blogabet.com", "standard")
    else:
        switchToTab()

    if task == "get_urls":
        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')

        # Use the collection 'my_collection'
        tips_coll = my_schema.get_collection('tips')
        tipster_coll = my_schema.get_collection('tipsters')
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)

        tips = tips_coll.find(
            f'result = "" AND eventDate < {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - (3600*7)}').execute()

        tips = tips.fetch_all()
        print(tips)
        tip_urls = []
        tipster_urls = []
        for tip in tips:
            tipster = tipster_coll.find(f'_id = "{tip["tipster"]}"').execute()
            tipster = tipster.fetch_one()
            tip_urls.append(tip['url'])
            if tipster['url'] not in tipster_urls:
                tipster_urls.append(tipster['url'])
        my_session.close()
        return tipster_urls
    elif task == "get_data":
        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')

        # Use the collection 'my_collection'
        tips_coll = my_schema.get_collection('tips')
        db_tips = tips_coll.find(
            f'result = "" AND eventDate < {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - (3600*7)}').execute()

        tips = tips.fetch_all()

        tip_urls = []
        for tip in db_tips:
            tip_urls.append(tip['"url'])

        go_to_archive_page()

        html = self.drivers[self.tabs['blogabet_results']['driver']
                            ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

        tip_soup = self.makeSoup(html)

        tips = tip_soup.find("ul", {"id": "feed-list"}).find_all(
            'li', {"class": "feed-pick"})

        for tip in tips:
            result = tip.find(
                'div', {'class': 'labels'}).find_all("i", {'class': "fa"})
            if len(result) > 0:

                print(result)
                result = result[-1]
                try:
                    resultString = result['data-original-title'].lower().capitalize()
                    print(resultString, url)

                    sendTipResult(resultString, url)
                except KeyError:
                    pass
