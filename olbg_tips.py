import json
from pprint import pprint
import selenium
from dotenv import load_dotenv
import re
import os
from time import sleep
from datetime import datetime, date, timedelta
import random
from gql import gql
import unidecode
import pytz
import traceback, requests, socket

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)


BLOGABET_USER = os.environ.get("BLOGABET_USER")
BLOGABET_PASS = os.environ.get("BLOGABET_PASS")
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


tipsters = [{
    "name": "Fdavidfbe",
    "url": "https://www.olbg.com/best-tipsters/Motor_Racing/14/507212",
    "sport": "F1"

}, {
    "name": "Analytik",
    "url": "https://www.olbg.com/best-tipsters/American_Football/11/602913",
    "sport": "American Football"

}, {
    "name": "Itspossible",
    "url": "https://www.olbg.com/best-tipsters/Rugby_League/10/592121",
    "sport": "Rugby League"

}, {
    "name": "JJOKER13",
    "url": "https://www.olbg.com/best-tipsters/Football/1/620280",
    "sport": "Football"

}, {
    "name": "Sheilatyrrell",
    "url": "https://www.olbg.com/best-tipsters/Aussie_Rules/22/617754",
    "sport": "Aussie Rules"

}, {
    "name": "Babalee",
    "url": "https://www.olbg.com/best-tipsters/Football/1/541277",
    "sport": "Football"

}, {
    "name": "The rascal",
    "url": "https://www.olbg.com/best-tipsters/American_Football/11/386851",
    "sport": "American Football"

}, {
    "name": "Ramchen",
    "url": "https://www.olbg.com/best-tipsters/Rugby_Union/9/5299",
    "sport": "Rugby Union"

}, {
    "name": "Dave1962",
    "url": "https://www.olbg.com/best-tipsters/Football/1/544617",
    "sport": "Football"

}, {
    "name": "Petethebet",
    "url": "https://www.olbg.com/best-tipsters/Ice_Hockey/13/92484",
    "sport": "Ice Hockey"

}, {
    "name": "Loqgiecruz",
    "url": "https://www.olbg.com/best-tipsters/Aussie_Rules/22/619167",
    "sport": "Aussie Rules"

}, {
    "name": "Rockabilly kat",
    "url": "https://www.olbg.com/best-tipsters/Boxing/16/253960",
    "sport": "Boxing"

}, {
    "name": "CJMST380",
    "url": "https://www.olbg.com/best-tipsters/Gaelic_Football/25/536106",
    "sport": "Gaelic Football"

}, {
    "name": "Rockabilly kat",
    "url": "https://www.olbg.com/best-tipsters/Rugby_League/10/253960",
    "sport": "Rugby League"

}, {
    "name": "Milasantiago",
    "url": "https://www.olbg.com/best-tipsters/Aussie_Rules/22/378852",
    "sport": "Aussie Rules"

}, {
    "name": "Fredgarvx",
    "url": "https://www.olbg.com/best-tipsters/American_Football/11/484296",
    "sport": "American Football"

}, {
    "name": "Forthegame",
    "url": "https://www.olbg.com/best-tipsters/American_Football/11/603515",
    "sport": "American Football"

}, {
    "name": "Fredgarvx",
    "url": "https://www.olbg.com/best-tipsters/Aussie_Rules/22/484296",
    "sport": "Aussie Rules"

}, {
    "name": "Cpadden",
    "url": "https://www.olbg.com/best-tipsters/Baseball/12/492692",
    "sport": "Baseball"

}, {
    "name": "Defiance1982",
    "url": "https://www.olbg.com/best-tipsters/Football/1/602760",
    "sport": "Football"

}]


def olbg_tips(self, task, url=None):

    def compileAndSendTip(tip):
        print(tip)
        tipString = ""
        betString = ""
        fixturesString = ""
        for bet in tip['tipBets']:
            if "eventID" in bet:
                betString = betString + f'''
                {{
                    market: "{bet["market"]}",
                    bet: "{bet["bet"]}",
                    eventID: "{bet["eventID"]}",
                    eventType: "{bet["eventType"]}",
                }},'''
            else:
                betString = betString + f'''
                {{
                    market: "{bet["market"]}",
                    bet: "{bet["bet"]}",
                    eventType: "{bet["eventType"]}",
                }},'''
        for fixture in tip['fixtureDets']:
            if "eventID" in fixture:
                fixturesString = fixturesString + f'''
                {{
                    sport: "{fixture["sport"]}",
                    region: "",
                    eventDate: {int(fixture["event_date"]) if fixture["event_date"] != "" else int(datetime.now().timestamp())},
                    homeTeam: "{fixture["homeTeam"]}",
                    awayTeam: "{fixture["awayTeam"]}",
                    eventID: "{fixture["eventID"]}",
                    competition: "{fixture["competition"]}"
                }},'''
            else:
                fixturesString = fixturesString + f'''
                {{
                    sport: "{fixture["sport"]}",
                    region: "",
                    eventDate: {int(fixture["event_date"]) if fixture["event_date"] != "" else int(datetime.now().timestamp())},
                    homeTeam: "{fixture["homeTeam"]}",
                    awayTeam: "{fixture["awayTeam"]}",
                    competition: "{fixture["competition"]}"
                }},'''

        print(tip['fixtureDets'])
        if len(tip['fixtureDets']) > 1:
            platformCategory = "Combo"
            platformSport = "Combo"
        else:
            platformCategory = f'{tip["fixtureDets"][0]["sport"]}-{tip["fixtureDets"][0]["competition"]}'
            platformSport = tip["fixtureDets"][0]["sport"]
        tipString = tipString + f'''{{
            platformCategory: "{platformCategory}",
            platformSport: "{platformSport}",
            tipDate: {int(datetime.timestamp(datetime.now()))}
            platform: "olbg",
            valid: true,
            events: [{fixturesString}],
            tipster: "{tip['tipster']['url']}",
            originalText: "{bet["market"]} {bet['bet']}",
            bets: [{betString}],
            odds: {tip["odds"]},
            live: {"true" if tip['event_status'] else "false"}
            units: {tip["stake"]},
            url: "{tip["url"]}",
        }},'''

        tipsterString = f"""{{
                    url: "{tip['tipster']['url']}",
                    profilePic: "{tip['tipster']['profilePic']}",
                    platform: "olbg",
                    handle: "{tip['tipster']['handle']}",
                }}"""
        query = f"""
            mutation{{
                updateTipster( inputTipster: {tipsterString} ) 
                addTip( inputTip: {tipString} ) {{_id}}
                
            }}
            """
        print(query)
        self.sendToApi(query)

    def sendTipResult(resultString, url):
        query = f"""
            mutation {{
                updateTip( inputTip: {{
                    url: "{url}",
                    result: "{resultString}"
                }} )

            }}
        """
        print(query)
        self.sendToApi(query)

    def getTeams(event):
        if " vs " in event.lower():
            teams = event.split(" vs ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])

        elif " - " in event.lower() or " – " in event.lower():
            teams = event.split(" - ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " -vs- " in event.lower():
            teams = event.split(" -vs- ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " vs. " in event.lower():
            teams = event.split(" vs. ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " @ " in event.lower():
            teams = event.split(" @ ")
            return self.stringCleaner(teams[1]), self.stringCleaner(teams[0])
        elif " v " in event.lower():
            teams = event.split(" v ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        else:
            return False, False

    def switchToTab():
        print("switching tab")
        self.drivers[self.tabs['olbg']['driver']
                     ]['driver'].switch_to.window(self.tabs['olbg']['handle'])
        refreshChecker = random.randint(1, 100)
        if refreshChecker >= 94:
            self.drivers[self.tabs['olbg']['driver']
                         ]['driver'].refresh()
            sleep(2)

    # get headers
    try:
        if task == "get_urls":
            return tipsters

        elif task == "get_data":
            print(url)
            tipster = url
            print(tipster)
            if "olbg" not in self.tabs:
                print("opening")
                self.openURL(
                    "olbg", "https://www.olbg.com/betting-tips", "standard")
            else:
                switchToTab()

            if 'olbg_trades' not in self.state:
                self.state['olbg_trades'] = {}

            todays_date_string = date.today().strftime("%Y-%m-%d")

            while True:
                try:
                    self.drivers[self.tabs['olbg']['driver']
                                ]['driver'].get(tipster['url'])
                    break
                except selenium.common.exceptions.TimeoutException:
                    sleep(2)
                    continue

            sleep(2)
            html = self.drivers[self.tabs['olbg']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            main_soup = self.makeSoup(html)
            try:
                tips = main_soup.find('table', {'id': 'tipsterListingContainer'}).find(
                    "tbody").find_all("tr")
            except AttributeError:
                return

            print(tips)
            if 'olbg_tips' not in self.state:
                self.state['olbg_tips'] = {}
            # loop through races
            new_tips_urls = []
            tips_details = []
            for tip in tips:

                naive = datetime.now()
                timezone = pytz.timezone("Europe/London")
                aware1 = timezone.localize(naive)
                print(tip['class'])

                if 'sport-tips-table-comments-row' not in tip['class']:
                    new_tips_urls.append(tip['data-tid'])
                    if tipster['name'] in self.state['olbg_tips'] and tip['data-tid'] in self.state['olbg_tips'][tipster['name']]:
                        continue
                    fixture = {}
                    bet = {}

                    fixture['sport'] = tipster['sport']

                    fixture['event'] = tip.find(
                        "a", class_="selection-event-link").text.strip()
                    bet['market'] = tip.find("div", class_="market-name").text
                    bet['bet'] = tip.find("h4", class_="selection-name").text
                    homeTeam, awayTeam = getTeams(
                        unidecode.unidecode(fixture['event']))
                    fixture['url'] = f'https://www.olbg.com{tip.find("a", class_="selection-event-link")["href"]}'
                    if homeTeam is False:
                        continue

                    tip_data = {
                        'url': tip['data-tid'],
                        'odds': float(tip.find("div", class_="odds-betting-btn-holder").find("span").text),
                        'event_status': False,
                        'type': "Single",
                        'tipBets': [bet],
                        'fixtureDets': [fixture],
                        "originalText": tip.find("div", {"class":"text-left"}).text,
                        "stake": 1,
                        "tipster": {
                            'url': tipster['url'].split("/")[-1],
                            'profilePic': "",
                            'platform': "olbg",
                            'handle': tipster['name'],
                            'tipsterType': "tipster"
                        }

                    }
                else:
                    try:
                        pprint(tip_data)
                        tips_details.append(tip_data)
                    except UnboundLocalError:
                        continue

            for t in tips_details:

                while True:
                    try:
                        self.drivers[self.tabs['olbg']['driver']
                                    ]['driver'].get(t['fixtureDets'][0]['url'])
                        break
                    except selenium.common.exceptions.TimeoutException:
                        sleep(2)
                        continue

                sleep(2)
                html = self.drivers[self.tabs['olbg']['driver']
                                    ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

                # turn page into soup
                main_soup = self.makeSoup(html)
                print(main_soup)
                try:
                    ts = int(main_soup.find(
                        "span", class_="event-start-time")['data-timestamp'])
                except ValueError:
                    continue

                try:
                    comp = main_soup.find(
                        "div", {"class": "tips-page-event-under-title-date"}).find("h2", {"class": "header-title-hr"}).text.strip()
                except AttributeError:
                    print(t['fixtureDets'][0]['url'])
                    while True:
                        try:
                            self.drivers[self.tabs['olbg']['driver']
                                        ]['driver'].get(t['fixtureDets'][0]['url'])
                            break
                        except selenium.common.exceptions.TimeoutException:
                            sleep(2)
                            continue
                    sleep(2)
                    html = self.drivers[self.tabs['olbg']['driver']
                                        ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")
                    main_soup = self.makeSoup(html)

                    comp = main_soup.find(
                        "div", {"class": "tips-page-event-under-title-date"}).find("h2", {"class": "header-title-hr"}).text.strip()

                # turn page into soup
                main_soup = self.makeSoup(html)
                homeTeam, awayTeam = getTeams(t['fixtureDets'][0]['event'])

                if fixture['sport'] == "Tennis":
                    h_array = homeTeam.split(" ", 1)
                    homeTeam = f"{h_array[1]} {h_array[0][0]}."

                    a_array = awayTeam.split(" ", 1)
                    awayTeam = f"{a_array[1]} {a_array[0][0]}."

                query = f"""
                    query{{
                        normalizeBets(query: "{{\\"events\\" : [{{\\"homeTeam\\":\\"{homeTeam.strip()}\\", \\"awayTeam\\":\\"{awayTeam.strip()}\\", \\"competition\\": \\"\\", \\"sport\\": \\"{t['fixtureDets'][0]['sport']}\\" }}], \\"bets\\":[{{\\"market\\": \\"{t['tipBets'][0]["market"]}\\", \\"bet\\":\\"{t['tipBets'][0]["bet"]}\\"}}],\\"platform\\":\\"olbg\\"}}") {{
                            market
                            bet
                        }}
                    }}
                """
                print(query)
                bets = self.sendToApi(query)
                t['tipBets'] = bets['data']['normalizeBets']

                if t['fixtureDets'][0]['sport'] == "esports":
                    eventType = "eSports"
                    event = self.matcher("collection",
                                        'esports_events',
                                        f"eventDate > {ts-3600} AND eventDate < {ts+3600} AND sport = '{t['fixtureDets'][0]['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
                else:
                    eventType = "sports"
                    event = self.matcher("collection",
                                        'sports_events',
                                        f"eventDate > {ts-3600} AND eventDate < {ts+3600} AND sport = '{t['fixtureDets'][0]['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
                print(event)
                if event is False:
                    t['fixtureDets'][0]['event_date'] = ts
                    t['fixtureDets'][0]['homeTeam'] = homeTeam
                    t['fixtureDets'][0]['awayTeam'] = awayTeam
                    t['fixtureDets'][0]['competition'] = comp
                else:
                    t['fixtureDets'][0]['event_date'] = event.iloc[0]['eventDate']
                    t['fixtureDets'][0]['homeTeam'] = event.iloc[0]['homeTeam']
                    t['fixtureDets'][0]['awayTeam'] = event.iloc[0]['awayTeam']
                    t['fixtureDets'][0]['eventID'] = event.iloc[0]['_id']
                    t['fixtureDets'][0]['competition'] = event.iloc[0]['competition']
                    t['tipBets'][0]['eventID'] = event.iloc[0]["_id"]
                t['tipBets'][0]['eventType'] = eventType

                compileAndSendTip(t)
            sleep(4)
        self.state['olbg_tips'][tipster['name']] = new_tips_urls

        self.saveState()

        # send to api
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            ## getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\","\\\\")
            err = traceback.format_exc().replace("\\","\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                        errorTitle: "Getting Tip From OLBG",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False

    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Tip From OLBG",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
