import pytz
from datetime import datetime, timedelta, date
import selenium
from time import sleep
from PIL import Image
import pytesseract
import cssutils
from cleantext import clean

tipsters = [{
    "name": "Thi.PamBets",
    "url": "https://www.tipsterland.es/tipsters/_usero20S1",

}, {
    "name": "Rodrigo",
    "url": "https://www.tipsterland.es/tipsters/RodriBet",

}, {
    "name": "BittPoint",
    "url": "https://www.tipsterland.es/tipsters/BitPoint",

}, {
    "name": "BETTIPS",
    "url": "https://www.tipsterland.es/tipsters/Bettips365",

}, {
    "name": "Mr.Tips Tennis",
    "url": "https://www.tipsterland.es/tipsters/Mr_TipsTennis",
}, {
    "name": "TENIS DE MESA TOP",
    "url": "https://www.tipsterland.es/tipsters/_user1nkLH",

}, {
    "name": "cetfy",
    "url": "https://www.tipsterland.es/tipsters/cetfy",

}, {
    "name": "MARTETIS",
    "url": "https://www.tipsterland.es/tipsters/MARTETIS",

}, {
    "name": "KPanalistas",
    "url": "https://www.tipsterland.es/tipsters/KPanalistas",

}]


sports_ref = {

    "1": "Tennis",
    "2": "Football",
    "3": "Basketball",
    "4": "Baseball",
    "5": "Badminton",
    "6": "Boxing",
    "7": "Cycling",
    "8": "Darts",
    "9": "eSports",
    "10": "F1",
    "11": "Greyhounds",
    "12": "Handball",
    "13": "Horse Racing",
    "14": "American Football",
    "15": "Volleyball",
    "16": "Ice Hockey",
    "17": "Pool",

}


def tipsterland(self, task, url=None):

    def switchToImageTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['tipsterland_image']['driver']
                         ]['driver'].switch_to.window(self.tabs['tipsterland_image']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "tipsterland_image", "https://www.tipsterland.es/", "standard")

    def processImage(tipElement):
        pick_id = tipElement.find(
            "button", {"class": "btn-pick-card-details"})['data-pick-id']
        image_button_url = f"https://www.tipsterland.es/api/dlg-pick-view/{pick_id}?dlgId=dlg_pick_card_details&show_tipster=false"

        if "tipsterland_image" not in self.tabs:
            print("opening")
            self.openURL(
                "tipsterland_image", "https://www.tipsterland.es/", "standard")
        else:
            switchToImageTab()
        self.drivers[self.tabs['tipsterland_image']['driver']
                     ]['driver'].get(image_button_url)

        image_url = self.drivers[self.tabs['tipsterland_image']['driver']
                                 ]['driver'].find_element(self.By.CLASS_NAME, "pick-image").get_attribute("href")

        with open('betslip.jpg', 'wb') as handle:
            response = self.request.get("standard", image_url, stream=True)

            if not response.ok:
                print(response)

            for block in response.iter_content(1024):
                if not block:
                    break

                handle.write(block)
        text = self.extractText("betslip.jpg").split("")
        return text

    def getTeams(event):
        if " vs " in event.lower():
            teams = event.split(" vs ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])

        elif " - " in event.lower() or " – " in event.lower():
            teams = event.split(" - ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " -vs- " in event.lower():
            teams = event.split(" -vs- ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " vs. " in event.lower():
            teams = event.split(" vs. ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])
        elif " @ " in event.lower():
            teams = event.split(" @ ")
            return self.stringCleaner(teams[1]), self.stringCleaner(teams[0])
        elif " v " in event.lower():
            teams = event.split(" v ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1])

    def betBuilder(tipElement, tip, event, betString):
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)
        fixture = {}
        bet = {}
        sport_number = tipElement.find(
            'img', {"class": 'x-sport-image'})['src'].split("/")[-1].split(".")[0]
        fixture['sport'] = sports_ref[sport_number]

        fixture['event'] = event

        tipDate = tipElement.find(
            "div", {"class": "pick-creation-time-time"}).text

        tip['url'] = f"{tipDate.lower().replace('/', '-').replace(' ', '-')}-{fixture['event'].replace('/', '-')}"

        event_date_string = tipElement.find(
            "div", {"class": "pick-card-event-date"}).find("span").text
        event_date_string = event_date_string.split(" ")
        if datetime.now().month == 12 and "Jan" in event_date_string:
            year = datetime.now().year + 1
        else:
            year = datetime.now().year
        event_date_string.insert(2, year)
        event_date_string = " ".join(event_date_string)
        element = datetime.strptime(event_date_string, "%d %b %Y %H:%M")
        fixture['event_date'] = int(datetime.timestamp(
            element)) + int(aware1.utcoffset().total_seconds())

        fixture['competition'] = self.translate(tipElement.find(
            "div", {"class": "pick-card-competition"}).text.strip(), "en"
        )
        tip['platform'] = ""

        tip['odds'] = float(tipElement.find(
            'div', {'class': 'odds-rounded-label'}).text.strip())
        tip['event_status'] = False
        homeTeam, awayTeam = getTeams(fixture['event'])
        if fixture['sport'] == "esports":
            eventType = "eSports"
            event = self.matcher("collection",
                                 'esports_events',
                                 f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
        else:
            eventType = "sports"
            event = self.matcher("collection",
                                 'sports_events',
                                 f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
        fixture['event_date'] = event.iloc[0]['eventDate']
        fixture['homeTeam'] = event.iloc[0]['homeTeam']
        fixture['awayTeam'] = event.iloc[0]['awayTeam']
        fixture['eventID'] = event.iloc[0]['_id']
        fixture['competition'] = event.iloc[0]['competition']
        tip['type'] = 'Betbuilder'
        tip['fixtureDets'] = [fixture]
        betStringList = betString.split(" + ")
        bets = []
        for bet in betStringList:
            bets.append({
                'market': bet,
                'bet': bet,
                'eventID': event.iloc[0]['_id'],
                'eventType': eventType
            })

        tip['tipBets'] = bets
        print(tip)
        return tip

    def acca(tipElement, tip, events, betString):

        tip['sports'] = []
        tip['events'] = []
        tip['event_date'] = []
        tip['markets'] = []
        tip['bets'] = []
        tip['event_status'] = False
        tip['competitions'] = []
        tip['platform'] = ""
        tip['type'] = 'Acca'
        tip['odds'] = float(tipElement.find(
            'div', {'class': 'odds-rounded-label'}).text.strip())
        tipDate = tipElement.find(
            "div", {"class": "pick-creation-time-time"}).text
        tip['url'] = f"{tipDate.lower().replace('/', '-').replace(' ', '-')}-{fixture['event'].replace('/', '-')}"
        if tip['tipster']['handle'] == "KPanalistas":
            selections = betString.split(" - ")
        else:
            selections = betString.split(" + ")

        if tip['tipster']['handle'] == "KPanalistas":
            processImage(tipElement)
            print(events)
        else:
            events = events.split(" + ")
        print(selections)
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)
        tip['tipBets'] = []
        for index, selection in enumerate(selections):
            fixture = {}
            bet = {}

            fixture['competition'] = 'Combo'

            if tip['tipster']['handle'] == "BittPoint":
                fixture['sport'] = "Table Tennis"
            elif tip['tipster']['handle'] == "BETTIPS" or tip['tipster']['handle'] == "KPanalistas":
                fixture['sport'] = "Football"
            elif tip['tipster']['handle'] == "Mr.Tips Tennis" or tip['tipster']['handle'] == "MARTETIS":
                fixture['sport'] = "Tennis"
            fixture['event'] = events[index]

            homeTeam, awayTeam = getTeams(fixture['event'])
            if fixture['sport'] == "esports":
                eventType = "eSports"
                event = self.matcher("collection",
                                     'esports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
            else:
                eventType = "sports"
                event = self.matcher("collection",
                                     'sports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
            fixture['event_date'] = event['eventDate']
            fixture['homeTeam'] = event['homeTeam']
            fixture['awayTeam'] = event['awayTeam']
            fixture['competition'] = event['competition']

            bet['market'] = selection
            bet['bet'] = selection
            bet['eventID'] = event['"_id']
            bet['eventType'] = eventType
            tip['tipBets'].append(bet)
            tip['fixtureDets'].append(fixture)
        return tip

    def single(tipElement, tip, event, betString):
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)
        fixture = {}
        bet = {}

        if event.lower() == "imagen":
            event = processImage(tipElement)
            print(event)
        sport_number = tipElement.find(
            'img', {"class": 'x-sport-image'})['src'].split("/")[-1].split(".")[0]
        fixture['sport'] = sports_ref[sport_number]

        fixture['event'] = event

        tipDate = tipElement.find(
            "div", {"class": "pick-creation-time-time"}).text

        tip['url'] = f"{tipDate.lower().replace('/', '-').replace(' ', '-')}-{fixture['event'].replace('/', '-')}"

        event_date_string = tipElement.find(
            "div", {"class": "pick-card-event-date"}).find("span").text
        event_date_string = event_date_string.split(" ")
        if datetime.now().month == 12 and "Jan" in event_date_string:
            year = datetime.now().year + 1
        else:
            year = datetime.now().year
        event_date_string.insert(2, year)
        event_date_string = " ".join(event_date_string)
        element = datetime.strptime(event_date_string, "%d %b %Y %H:%M")
        fixture['event_date'] = int(datetime.timestamp(
            element)) + int(aware1.utcoffset().total_seconds())

        fixture['competition'] = self.translate(tipElement.find(
            "div", {"class": "pick-card-competition"}).text.strip(), "en"
        )
        tip['platform'] = ""

        tip['odds'] = float(tipElement.find(
            'div', {'class': 'odds-rounded-label'}).text.strip())
        tip['event_status'] = False

        bet['market'] = betString
        bet['bet'] = betString

        if fixture['sport'] == "Horse Racing":
            race = fixture['event'].split(" ", 1)[1].split("(")[0].strip()
            trade_query = f"""
                    mutation {{ newTrade(inputTradeAlert: {{
                        strategy: "Sentiment",
                        selection: "{bet['bet']}",
                        sport: "Horse Racing",
                        positionType: "Back",
                        event: "{race}",
                        starttime: {fixture['event_date']},
                    }})}}
                """
            result = self.sendToApi(trade_query)
            print(result)
            return "Trade"
        else:
            homeTeam, awayTeam = getTeams(fixture['event'])
            if fixture['sport'] == "esports":
                eventType = "eSports"
                event = self.matcher("collection",
                                     'esports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
            else:
                eventType = "sports"
                event = self.matcher("collection",
                                     'sports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam})
            fixture['event_date'] = event['eventDate']
            fixture['homeTeam'] = event['homeTeam']
            fixture['awayTeam'] = event['awayTeam']
            fixture['competition'] = event['competition']
            tip['type'] = 'Single'
            tip['fixtureDets'] = [fixture]
            bet['eventID'] = event["_id"]
            bet['eventType'] = eventType
            tip['tipBets'] = [bet]
            print(tip)
            return tip

    def getTipDetails(tipElement, tipster, tipster_profile_pic):
        tip = {
            "stake": tipElement.find(
                'div', {'class': 'stake-rounded-label'}).text.strip().split("/")[0],
            "tipster": {
                'url': tipster['url'],
                'profilePic': tipster_profile_pic,
                'platform': "tipsterland",
                'handle': tipster['name'],
                'tipsterType': "tipster"
            }
        }
        event = clean(tipElement.find(
            'div', {'class': 'pick-card-event'}).text.strip(), no_emoji=True)
        betString = self.translate(tipElement.find(
            'div', {'class': 'x-pick-alert'}).encode_contents().replace("r\n", "").replace("\n", "").replace("\r", "").strip().split('@')[0].strip(), "en")

        if " + " in event and " + " not in betString or (tip['tipster']['handle'] == "KPanalistas" and "combinada" in event.lower()):
            tip = acca(tipElement, tip, event, betString)
        elif " + " in betString and " + " not in event:
            tip = betBuilder(tipElement, tip, event, betString)
        else:
            tip = single(tipElement, tip, event, betString)

        return tip

    def compileAndSendTip(tip):
        print(tip)
        tipString = ""
        betString = ""
        fixturesString = ""
        for bet in tip['tipBets']:
            betString = betString + f'''
            {{
                market: "{bet["market"]}",
                bet: "{bet["bet"]}",
                eventID: "{bet["eventID"]}",
                eventType: "{bet["eventType"]}",
            }},'''
        for fixture in tip['fixtureDets']:

            fixturesString = fixturesString + f'''
            {{
                sport: "{fixture["sport"]}",
                region: "",
                eventID: "{fixture["eventID"]}",
                eventDate: {int(fixture["event_date"]) if fixture["event_date"] != "" else int(datetime.now().timestamp())},
                homeTeam: "{fixture["homeTeam"]}",
                awayTeam: "{fixture["awayTeam"]}",
                competition: "{fixture["competition"]}"
            }},'''

        print(tip['fixtureDets'])
        if len(tip['fixtureDets']) > 1:
            platformCategory = "Combo"
            platformSport = "Combo"
        else:
            platformCategory = f'{tip["fixtureDets"][0]["sport"]}-{tip["fixtureDets"][0]["competition"]}'
            platformSport = tip["fixtureDets"][0]["sport"]
        tipString = tipString + f'''{{
            platformCategory: "{platformCategory}",
            platformSport: "{platformSport}",
            tipDate: {int(datetime.timestamp(datetime.now()))}
            platform: "tipsterland",
            valid: true,
            events: [{fixturesString}],
            tipster: "{tip['tipster']['url']}",
            originalText: "{bet["market"]} {bet['bet']}",
            bets: [{betString},],
            odds: {tip["odds"]},
            live: {"true" if tip['event_status'] else "false"}
            units: {tip["stake"]},
            url: "{tip["url"]}",
        }},'''

        tipsterString = f"""{{
                    url: "{tip['tipster']['url']}",
                    profilePic: "{tip['tipster']['profilePic']}",
                    platform: "tipsterland",
                    handle: "{tip['tipster']['handle']}"
                }}"""
        query = f"""
            mutation {{
             updateTipster( inputTipster: {tipsterString} ) 
                addTip( inputTips: {tipString} ) {{_id}}
               
            }}
            """
        print(query)
        self.sendToApi(query)

    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['tipsterland']['driver']
                         ]['driver'].switch_to.window(self.tabs['tipsterland']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "tipsterland", "https://www.tipsterland.es/", "standard")

    def sendTipResult(reultString, url):
        query = f"""
            mutation {{
                updateTip( inputTip: {{
                    url: "{url}",
                    result: "{resultString}"
                }} )

            }}
        """
        print(query)
        self.sendToApi(query)

    if "tipsterland" not in self.tabs:
        print("opening")
        self.openURL(
            "tipsterland", "https://www.tipsterland.es/", "standard")
    else:
        switchToTab()

    if task == "get_urls":
        return tipsters

    elif task == "get_data":
        print(url)
        tipster = url
        print(tipster)
        if "tipsterland" not in self.tabs:
            print("opening")
            self.openURL(
                "tipsterland", "https://www.tipsterland.es/", "standard")
        else:
            switchToTab()

        if 'tipsterland_tips' not in self.state:
            self.state['tipsterland_tips'] = {}

        new_tips_urls = []
        tipDetailsList = []

        self.drivers[self.tabs['olbg']['driver']
                     ]['driver'].get(tipster['url'])

        sleep(2)
        html = self.drivers[self.tabs['olbg']['driver']
                            ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

        # turn page into soup
        main_soup = self.makeSoup(html)

        tips = main_soup.find_all('div', {'class': 'x-pick-card'})
        tipster_profile_pic = main_soup.find(
            "div", {"class": "public-profile-tipster-image"})['style']
        style = cssutils.parseStyle(tipster_profile_pic)
        tipster_profile_pic = style['background-image']
        tipster_profile_pic = url.replace('url(', '').replace(')', '')
        print(tips)
        for tip in tips:
            result = tip['class']
            print(result)
            if "pick-card-warning" not in result:
                result = result.split("-")[-1]
            url = tip.find(
                'div', {'class': 'feed-pick-title'}).find("h3").find("a")["href"].strip()
            if result is not None:
                if result == "success":
                    resultString = "Win"
                elif result == "danger":
                    resultString = "Loss"

                print(resultString, url)

                sendTipResult(resultString)
                new_tips_urls.append(url)
                continue
            if url in self.state['tipsterland_tips']:
                continue
            tipDetails = getTipDetails(tip, tipster, tipster_profile_pic)
            if tipDetails is not False:
                if tipDetails != "Trade":
                    tipDetailsList.append(tipDetails)

            new_tips_urls.append(url)
        for tip in tipDetailsList:
            compileAndSendTip(tip)
    self.state['tipsterland_tips'][tipster['name']] = new_tips_urls

    self.saveState()
