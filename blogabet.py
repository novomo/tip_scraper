import selenium
from dotenv import load_dotenv
import re
import os
from time import sleep
from datetime import datetime
import random
import unidecode
import pytz
import json
import pycountry
import traceback, requests, socket
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

PROXY = os.environ.get("PROXY")
BLOGABET_USER = os.environ.get("BLOGABET_USER")
BLOGABET_PASS = os.environ.get("BLOGABET_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}

def blogabet(self, task, url=None):

    def getTeams(event):
        if " vs " in event.lower():
            teams = event.split(" vs ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]

        elif " - " in event.lower():
            teams = event.split(" - ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]
        elif " – " in event.lower():
            teams = event.split(" – ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]
        elif " -vs- " in event.lower():
            teams = event.split(" -vs- ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]
        elif " vs. " in event.lower():
            teams = event.split(" vs. ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]
        elif " @ " in event.lower():
            teams = event.split(" @ ")
            return self.stringCleaner(teams[1]), self.stringCleaner(teams[0]), teams[1], teams[0]
        elif " v " in event.lower():
            teams = event.split(" v ")
            return self.stringCleaner(teams[0]), self.stringCleaner(teams[1]), teams[0], teams[1]

    def switchToTab():
        print("switching tab")
        self.drivers[self.tabs['blogabet']['driver']
                     ]['driver'].switch_to.window(self.tabs['blogabet']['handle'])
        refreshChecker = random.randint(1, 100)
        if refreshChecker >= 94:
            self.drivers[self.tabs['blogabet']['driver']
                         ]['driver'].refresh()
            sleep(2)

    def checkLogin():
        print(dotenv_path)
        self.headlessClick("//*[contains(text(), 'over 18')]",
                           self.By.XPATH, waitTime=2, driverKey='standard')
        logged_out = self.headlessWaitForElements(
            "present", "form-login-div-email", self.By.ID, waitTime=1, driverKey='standard')
        print(logged_out)
        if len(logged_out) > 0:

            usernameInput = self.drivers[self.tabs['blogabet']['driver']]['driver'].find_element(
                self.By.ID, "email"

            )
            usernameInput.clear()
            usernameInput.send_keys(self.Keys.CONTROL, "a")
            usernameInput.send_keys(self.Keys.BACKSPACE)
            self.typeText(BLOGABET_USER, usernameInput)
            passwordInput = self.drivers[self.tabs['blogabet']['driver']]['driver'].find_element(
                self.By.NAME, "password"
            )
            passwordInput.send_keys(self.Keys.CONTROL, "a")
            passwordInput.send_keys(self.Keys.BACKSPACE)
            passwordInput.clear()
            self.typeText(BLOGABET_PASS, passwordInput)
            sleep(2)
            self.drivers[self.tabs['blogabet']['driver']]['driver'].find_element(
                self.By.CLASS_NAME, "form-actions").find_element(
                self.By.TAG_NAME, "button").click()
            sleep(2)
            iframes = self.drivers[self.tabs['blogabet']['driver']]['driver'].find_elements(
                self.By.TAG_NAME, "iframe")
            for iframe in iframes:
                try:
                    self.drivers[self.tabs['blogabet']['driver']
                                 ]['driver'].switch_to.frame(iframe)
                    here = self.headlessWaitForElements(
                        "present", "solver-button", self.By.ID, waitTime=1, driverKey='standard')
                    if len(here) > 0:
                        here[0].click()

                    sleep(3)
                    self.drivers[self.tabs['blogabet']['driver']
                                 ]['driver'].switch_to.default_content()
                except selenium.common.exceptions.StaleElementReferenceException:
                    pass

    def blogabetKeepAlive():

        self.moveTo(500, 200)
        self.click()
        self.scroll(100)
        # scroll
        scroll = random.randint(5, 10)
        self.scroll(-scroll)   # scroll up 10 "clicks"
        self.scroll(scroll*2)

        # if not solved
        # close tab and refresh stream
        # else random refresh
        sleep(1)
        refreshChecker = random.randint(1, 100)
        if refreshChecker > 94:
            self.drivers[self.tabs['blogabet']['driver']
                         ]['driver'].find_element(
                self.By.ID, "reloadFeedBtn").click()

    def acca(tipElement, tip):

        tip['sports'] = []
        tip['events'] = []
        tip['event_date'] = []
        tip['markets'] = []
        tip['bets'] = []
        tip['event_status'] = False
        tip['competitions'] = []
        tip['platform'] = tipElement.find(
            'div', {'class': 'labels'}).find('a').text.strip()
        tip['type'] = 'Acca'
        tip['odds'] = tipElement.find(
            'div', {'class': 'pick-line'}).find(
            'span', {'class': 'feed-odd'}).text.strip()
        tip['url'] = tipElement.find(
            'div', {'class': 'feed-pick-title'}).find("h3").find("a")["href"].strip()
        selections = tipElement.find(
            "table", {"class": 'combo-table'}).find_all('tr')
        print(selections)
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)
        tip['tipBets'] = []
        tip['fixtureDets'] = []
        originalText = ""
        for selection in selections:
            originalText = f"{originalText} {selection.text}"
            fixture = {}
            bet = {}
            tds = selection.find_all('td')
            if len(tds) == 0:
                continue
            fixture['competition'] = 'Combo'

            if tds[0].find('div')['class'][1].lower() == 'am. Football':
                fixture['sport'] = "American Football"
            else:
                fixture['sport'] = tds[0].find('div')['class'][
                    1].replace('-', ' ').capitalize()

            fixture['event'] = tds[1].text

            bet['market'] = tds[2].text.split('(', 1)[-1].replace(')', '')
            bet['bet'] = tds[2].text.rsplit('(', 1)[0].strip()

            homeTeam, awayTeam, org_homeTeam, org_awayTeam = getTeams(
                unidecode.unidecode(fixture['event'].replace("(", "").replace(")", "")))

            query = f"""
                query{{
                    normalizeBets(query: "{{\\"events\\" : [{{\\"homeTeam\\":\\"{org_homeTeam.strip()}\\", \\"awayTeam\\":\\"{org_awayTeam.strip()}\\", \\"competition\\": \\"\\", \\"sport\\": \\"{fixture['sport']}\\" }}], \\"bets\\":[{{\\"market\\": \\"{bet["market"]}\\", \\"bet\\":\\"{bet["bet"]}\\"}}],\\"platform\\":\\"blogabet\\"}}") {{
                        market
                        bet
                    }}
                }}
            """

            bets = self.sendToApi(query)
            bet = bets['data']['normalizeBets'][0]

            if fixture['sport'] == "Tennis":
                homeTeam = homeTeam.replace("[f]", "").strip()
                awayTeam = awayTeam.replace("[f]", "").strip()
                print(homeTeam)
                h_array = homeTeam.split(" ", 1)
                print(h_array)
                if len(h_array) == 1:
                    homeTeam = f"{h_array[0]}"
                else:

                    homeTeam = f"{h_array[1]} {h_array[0][0]}."

                a_array = awayTeam.split(" ", 1)

                if len(a_array) == 1:
                    awayTeam = f"{a_array[0]}"
                else:

                    awayTeam = f"{a_array[1]} {a_array[0][0]}."

            if fixture['sport'] == "esports":
                eventType = "eSports"
                event = self.matcher("collection",
                                     'esports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
            else:
                eventType = "sports"
                event = self.matcher("collection",
                                     'sports_events',
                                     f"eventDate > {int(datetime.timestamp(datetime.now())) - int(aware1.utcoffset().total_seconds()) - 3600} AND sport = '{fixture['sport']}'", {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
            print(event)
            if event is False:
                fixture['event_date'] = 0
                fixture['homeTeam'] = homeTeam
                fixture['awayTeam'] = awayTeam

                fixture['competition'] = fixture['sport']
            else:
                fixture['event_date'] = event.iloc[0]['eventDate']
                fixture['homeTeam'] = event.iloc[0]['homeTeam']
                fixture['awayTeam'] = event.iloc[0]['awayTeam']
                fixture['eventID'] = event.iloc[0]['_id']
                fixture['competition'] = event.iloc[0]['competition']
                bet['eventID'] = event.iloc[0]['_id']
            accaMarketsArray = tds[2].text.split('(')
            tip['originalText'] = originalText.replace("\n", "")
            bet['eventType'] = eventType
            tip['tipBets'].append(bet)
            tip['fixtureDets'].append(fixture)
        return tip

    def single(tipElement, tip):
        naive = datetime.now()
        timezone = pytz.timezone("Europe/London")
        aware1 = timezone.localize(naive)
        fixture = {}
        bet = {}
        fixture['sport'] = tipElement.find(
            'div', {"class": 'sport-line'}).find('span')["data-original-title"]
        if fixture['sport'] == 'Am. Football':
            fixture['sport'] = "American Football"

        fixture['event'] = tipElement.find(
            'div', {'class': 'feed-pick-title'}).find("h3").find("a").text.strip()
        tip['url'] = tipElement.find(
            'div', {'class': 'feed-pick-title'}).find("h3").find("a")['href']
        timeStringArray = tipElement.find(
            'div', {"class": 'sport-line'}).find(
            "small", {"class": 'text-muted'}).decode_contents().strip().split('/')

        timeString = timeStringArray[-1].split(">")[1].replace(",", '').strip()
        print(timeString)
        element = datetime.strptime(timeString, "%d %b %Y %H:%M")
        fixture['event_date'] = int(datetime.timestamp(
            element)) + int(aware1.utcoffset().total_seconds())
        fixture['competition'] = timeStringArray[2].split('>')[1].strip()
        tip['platform'] = tipElement.find(
            'div', {'class': 'feed-pick-title'}).find(
            'div', {'class': 'labels'}).find('a').decode_contents().strip()

        tip['odds'] = tipElement.find(
            'div', {'class': 'pick-line'}).find(
            'span', {'class': 'feed-odd'}).decode_contents().strip()

        if "not verified!" in tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip().split('(')[0]:
            tip['event_status'] = False
            bet['market'] = tipElement.find(
                'div', {'class': 'pick-line'}).text.replace("r\n", "").replace("\n", "").replace("\r", "").strip().split('@')[0].strip()
            bet['bet'] = tipElement.find(
                'div', {'class': 'pick-line'}).text.replace("r\n", "").replace("\n", "").replace("\r", "").strip().split('@')[0].strip()

        elif len(tipElement.find(
            'div', {'class': 'labels'}).find(
                'span')) == 1:
            tip['event_status'] = False
            marketsArray = tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip().split('(')
            bet['market'] = marketsArray[-1].replace(')',
                                                     '').split('@')[0].strip()
            betsArray = tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip().rsplit(' (', 1)[0].split(">")
            bet['bet'] = betsArray[-1].strip()

        elif 'LIVE' in tipElement.find(
            'div', {'class': 'labels'}).find(
                'span')[-1].decode_contents().strip():
            tip['event_status'] = True
            marketsText = tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip()
            if ') (' not in marketsText:
                marketsArray = marketsText.split('(')
                bet['market'] = marketsArray[-1].replace(')',
                                                         '').split('@')[0].strip().replace("amp;", "")
            elif ') (' in marketsText:
                marketsArray = marketsText.split(') (')[0].rsplit('(', 2)
                bet['market'] = marketsArray[-2].split(
                    '(')[0].strip().replace("amp;", "")
            else:
                bet['market'] = marketsText.split(
                    ') (')[0].rsplit('(', 1)[1].strip().replace("amp;", "")

            if marketsText.startswith('('):
                betsArray = marketsText.split(')')[1].split(
                    '(', 1)[0].split(">")
                bet['bet'] = betsArray[-1].strip()
            elif ')) (' in marketsText:
                betsArray = marketsText.split(
                    '(')[0].split('(', 1)[0].split(">")
                bet['bet'] = betsArray[-1].strip()
            else:
                betsArray = marketsText.split(
                    ') (')[0].rsplit('(', 1)[0].split(">")
                bet['bet'] = betsArray[-1].strip()

        else:
            tip['event_status'] = False
            marketsArray = tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip().split('(')
            bet['market'] = marketsArray[-1].replace(')',
                                                     '').split('@')[0].strip().replace("amp;", "")
            betsArray = tipElement.find(
                'div', {'class': 'pick-line'}).decode_contents().strip().rsplit(' (', 1)[0].split(">")
            bet['bet'] = betsArray[-1].strip()

        if fixture['sport'] == "Horse Racing":
            race = fixture['event'].split(" ", 1)[1].split("(")[0].strip()
            trade_query = f"""
                    mutation {{ newTrade(inputTradeAlert: {{
                        strategy: "Sentiment",
                        selection: "{bet['bet']}",
                        sport: "Horse Racing",
                        positionType: "Back",
                        event: "{race}",
                        starttime: {fixture['event_date']},
                    }})}}
                """
            result = self.sendToApi(trade_query)
            print(result)
            return "Trade"
        else:
            try:
                homeTeam, awayTeam, org_homeTeam, org_awayTeam = getTeams(
                    unidecode.unidecode(fixture['event'].replace("(", "").replace(")", "")))
            except TypeError:
                return False

            query = f"""
                query{{
                    normalizeBets(query: "{{\\"events\\" : [{{\\"homeTeam\\":\\"{org_homeTeam.strip()}\\", \\"awayTeam\\":\\"{org_awayTeam.strip()}\\", \\"competition\\": \\"\\", \\"sport\\": \\"{fixture['sport']}\\" }}], \\"bets\\":[{{\\"market\\": \\"{bet["market"]}\\", \\"bet\\":\\"{bet["bet"]}\\"}}],\\"platform\\":\\"blogabet\\"}}") {{
                        market
                        bet
                    }}
                }}
            """
            print(query)
            bets = self.sendToApi(query)
            bet = bets['data']['normalizeBets'][0]

            if fixture['sport'] == "Tennis":
                homeTeam = homeTeam.replace("[f]", "").strip()
                awayTeam = awayTeam.replace("[f]", "").strip()
                print(homeTeam)
                h_array = homeTeam.split(" ", 1)
                print(h_array)
                if len(h_array) == 1:
                    homeTeam = f"{h_array[0]}"
                else:

                    homeTeam = f"{h_array[1]} {h_array[0][0]}."

                a_array = awayTeam.split(" ", 1)

                if len(a_array) == 1:
                    awayTeam = f"{a_array[0]}"
                else:

                    awayTeam = f"{a_array[1]} {a_array[0][0]}."

            fixture['region'] = ""
            for x in pycountry.countries:
                if x.name.lower() in fixture['competition'].lower():
                    fixture['region'] = f" {x.name} "
                    break
            if fixture['region'] == "":
                if "Bra." in fixture['competition']:
                    fixture['region'] = " Brazil "
                elif "Fra." in fixture['competition']:
                    fixture['region'] = " France "
                elif "Ger." in fixture['competition']:
                    fixture['region'] = " Germany "
                elif "Eng." in fixture['competition']:
                    fixture['region'] = " England "
            eventType = ""
            if fixture['sport'] == "Tennis":
                if "ATP" in fixture['competition']:
                    eventType = "Men"
                elif "WTA" in fixture['competition']:
                    eventType = "Men"
            if eventType == "" and fixture['competition'] == "Women" or "women" in homeTeam.lower():
                eventType = "Women"
            elif "youth" in homeTeam or re.compile("u\d\d", re.IGNORECASE).search(homeTeam):
                eventType = "Youth"

            if eventType != "" and fixture['region'] != "":
                q = f"eventDate > {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) - (3600*3)} AND eventDate < {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) + (3600*3)} AND sport = '{fixture['sport']}' and region = '{self.stringCleaner(fixture['region'])}' and eventType = '{eventType}'"
            elif eventType != "":
                q = f"eventDate > {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) - (3600*3)} AND eventDate < {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) + (3600*3)} AND sport = '{fixture['sport']}' and eventType = '{eventType}'"
            elif fixture['region'] != "":
                q = f"eventDate > {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) - (3600*3)} AND eventDate < {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) + (3600*3)} AND sport = '{fixture['sport']}' and region = '{self.stringCleaner(fixture['region'])}'"
            else:
                q = f"eventDate > {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) - (3600*3)} AND eventDate < {fixture['event_date'] - int(aware1.utcoffset().total_seconds()) + (3600*3)} AND sport = '{fixture['sport']}'"

            if fixture['sport'] == "esports":
                eventType = "eSports"
                event = self.matcher("collection",
                                     'esports_events',
                                     q, {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
            else:
                eventType = "sports"
                event = self.matcher("collection",
                                     'sports_events',
                                     q, {'homeTeam': homeTeam, 'awayTeam': awayTeam}, self.stringCleaner)
            if event is False:
                fixture['event_date'] = fixture['event_date']
                fixture['homeTeam'] = homeTeam
                fixture['awayTeam'] = awayTeam
                fixture['competition'] = fixture['competition']
            else:

                fixture['event_date'] = event.iloc[0]['eventDate']
                fixture['homeTeam'] = event.iloc[0]['homeTeam']
                fixture['awayTeam'] = event.iloc[0]['awayTeam']
                fixture['eventID'] = event.iloc[0]['_id']
                fixture['competition'] = event.iloc[0]['competition']
                bet['eventID'] = event.iloc[0]["_id"]
            bet['eventType'] = eventType
            tip['type'] = 'Single'
            print(tipElement.find(
                    'div', {'class': 'feed-pick-title'}).find("div").text)
            first = tipElement.find(
                    'div', {'class': 'feed-pick-title'}).find("div").text.replace("\n", " ").strip().rsplit("/", 3)[0][:-1]
            try:
                second = tipElement.find(
                        'div', {'class': 'feed-pick-title'}).find("div").text.replace("\n", " ").strip().split("/", 1)[1].split(" i ", 1)[1].replace(" Livestream ", "")
            except IndexError:
                second_split = tipElement.find(
                        'div', {'class': 'feed-pick-title'}).find("div").text.replace("\n", " ").strip().rsplit("/", 3)
                second = f"{second_split[1].rsplit(' ', 2)[-2]} {second_split[-1]}"
            tip['originalText'] = re.sub(r"[^\S\r\n]{2,}", " ",f'{first} {second}'.replace(" 1 ", ""))
            tip['fixtureDets'] = [fixture]
            tip['tipBets'] = [bet]
            print(tip)
            return tip

    def getTipDetails(tipElement):
        bet_placed = tipElement.find(
            'small', {'class': 'bet-age'}).text

        if re.search("\s\d\d\:\d\d/i", bet_placed):
            return False

        tip = {
            "stake": tipElement.find(
                'div', {'class': 'labels'}).find('span').text.strip().split("/")[0],
            "tipster": {
                'url': tipElement.find(
                    'div', {'class': 'title-name'}).find("a")["href"],
                'profilePic': tipElement.find(
                    'div', {'class': "feed-avatar"}).find("img")["src"],
                'platform': "blogabet",
                'handle': tipElement.find(
                    'div', {'class': "feed-avatar"}).find("a")["title"],
                'tipsterType': "tipster"
            }
        }
        if len(tipElement.find_all("table", {"class": 'combo-table'})) > 0:
            tip = acca(tipElement, tip)
        else:
            tip = single(tipElement, tip)
        return tip

    def compileAndSendTip(tip):
        print(tip)
        tipString = ""
        betString = ""
        fixturesString = ""
        for bet in tip['tipBets']:
            if "eventID" in bet:
                betString = betString + f'''
                {{
                    market: "{bet["market"]}",
                    bet: "{bet["bet"]}",
                    eventID: "{bet["eventID"]}",
                    eventType: "{bet["eventType"]}",
                }},'''
            else:
                betString = betString + f'''
                {{
                    market: "{bet["market"]}",
                    bet: "{bet["bet"]}",
                    eventType: "{bet["eventType"]}",
                }},'''
        for fixture in tip['fixtureDets']:
            if "eventID" in fixture:
                fixturesString = fixturesString + f'''
                {{
                    sport: "{fixture["sport"]}",
                    region: "",
                    eventDate: {int(fixture["event_date"]) if fixture["event_date"] != "" else int(datetime.now().timestamp())},
                    homeTeam: "{fixture["homeTeam"]}",
                    awayTeam: "{fixture["awayTeam"]}",
                    eventID: "{fixture["eventID"]}",
                    competition: "{fixture["competition"]}"
                }},'''
            else:
                fixturesString = fixturesString + f'''
                {{
                    sport: "{fixture["sport"]}",
                    region: "",
                    eventDate: {int(fixture["event_date"]) if fixture["event_date"] != "" else int(datetime.now().timestamp())},
                    homeTeam: "{fixture["homeTeam"]}",
                    awayTeam: "{fixture["awayTeam"]}",
                    competition: "{fixture["competition"]}"
                }},'''
        print(tip['fixtureDets'])
        if len(tip['fixtureDets']) > 1:
            platformCategory = "Combo"
            platformSport = "Combo"
        else:
            platformCategory = f'{tip["fixtureDets"][0]["sport"]}-{tip["fixtureDets"][0]["competition"]}'
            platformSport = tip["fixtureDets"][0]["sport"]
        tipString = tipString + f'''{{
            platformCategory: "{platformCategory}",
            platformSport: "{platformSport}",
            tipDate: {int(datetime.timestamp(datetime.now()))}
            platform: "blogabet",
            valid: true,
            events: [{fixturesString}],
            tipster: "{tip['tipster']['url']}",
            originalText: "{tip["originalText"]}",
            bets: [{betString},],
            odds: {tip["odds"]},
            live: {"true" if tip['event_status'] else "false"}
            units: {tip["stake"]},
            url: "{tip["url"]}",
        }},'''

        tipsterString = f"""{{
                    url: "{tip['tipster']['url']}",
                    profilePic: "{tip['tipster']['profilePic']}",
                    platform: "blogabet",
                    handle: "{tip['tipster']['handle']}"
                }}"""
        query = f"""
            mutation {{
             updateTipster( inputTipster: {tipsterString} ) 
                addTip( inputTip: {tipString} ) {{_id}}
               
            }}
            """
        print(query)
        self.sendToApi(query)

    def sendTipResult(reultString, url):
        query = f"""
            mutation {{
                updateTip( inputTip: {{
                    url: "{url}",
                    result: "{resultString}"
                }} ) {{
                _id
                }}

            }}
        """
        print(query)
        self.sendToApi(query)

    try:
        if task == "get_urls":
            return ["https://blogabet.com/#login"]

        if task == "get_data":
            if 'blogabet_tips' not in self.state:
                self.state['blogabet_tips'] = []
            if "blogabet" not in self.tabs:
                print("opening")
                self.openURL(
                    "blogabet", "https://blogabet.com/#login", "standard")
            else:
                switchToTab()

            checkLogin()
            blogabetKeepAlive()
            myTipstersElement = self.drivers[self.tabs['blogabet']['driver']
                                            ]['driver'].find_element(
                self.By.ID, "mytipsters_feed")
            self.webdriverScrollTo(
                myTipstersElement, self.tabs['blogabet']['driver'])
            self.headlessClick("mytipsters_feed", self.By.ID, driverKey='standard')
            sleep(3)

            self.headlessWaitForElements(
                "present", "feed-list", self.By.ID, driverKey="standard")
            html = self.drivers[self.tabs['blogabet']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            tip_soup = self.makeSoup(html)
            # print(tip_soup)

            tips = tip_soup.find("ul", {"id": "feed-list"}).find_all(
                'li', {"class": "feed-pick"})

            new_tips_urls = []
            tipDetailsList = []

            clickHere = self.drivers[self.tabs['blogabet']['driver']
                                    ]['driver'].find_elements(
                self.By.XPATH, "//a[contains(text(), 'here')]")
            print("click here")
            print(clickHere)
            '''
            if len(clickHere) > 0:

                print(clickHere)
                clickHere = clickHere[0]
                self.webdriverScrollTo(
                    clickHere, self.tabs['blogabet']['driver'])
                self.webdriverScrollTo(clickHere, driverKey='standard')
                self.headlessClick(
                    "//a[contains(text(), 'here')]", self.By.XPATH, driverKey='standard')
                checkbox = self.headlessWaitForElements(
                    "visible", "recaptcha-checkbox", self.By.CLASS_NAME, waitTime=3, driverKey='standard')
                checkbox[0].click()
                self.clickImage("buster", wait=4)
                got = False
                while True:
                    for i in range(6):
                        sleep(1)
                        if self.drivers[self.tabs['blogabet']['driver']]['driver'].find_element(self.By.CLASS_NAME, 'pick-line') is not None:
                            tipElements = self.drivers[self.tabs['blogabet']['driver']]['driver'].find_elements(
                                self.By.CLASS_NAME, 'feed-pick')
                            tipDetails = getTipDetails(tipElements[0])
                            if tipDetails is not False:
                                tipDetailsList.append(tipDetails)
                            self.drivers[self.tabs['blogabet']
                                        ['driver']]['driver'].close()

                            got = True
                            self.tabs['blogabet']['needsRefresh'] = True
                            break
                    if got:
                        break
                        '''
            for tip in tips:
                clickHere = self.drivers[self.tabs['blogabet']['driver']
                                        ]['driver'].find_elements(
                    self.By.XPATH, "//a[contains(text(), 'here')]")
                if len(clickHere) > 0:
                    continue
                if 'Sponsored link' in tip.find(
                        'small', {'class': 'bet-age'}).text:
                    continue
                if len(tip.find_all("div",
                                    {"class": "feed-comment"})) > 0:
                    continue
                result = tip.find(
                    'div', {'class': 'labels'}).find_all("i", {'class': "fa"})
                url = tip.find(
                    'div', {'class': 'feed-pick-title'}).find("h3").find("a")["href"].strip()
                new_tips_urls.append(url)
                if len(result) > 0:
                    print(result)
                    result = result[-1]
                    try:
                        resultString = result['data-original-title'].lower().capitalize()
                        print(resultString, url)

                        sendTipResult(resultString, url)

                        continue
                    except KeyError:
                        pass
                print(url in self.state['blogabet_tips'])
                if url in self.state['blogabet_tips']:
                    continue
                tipDetails = getTipDetails(tip)
                if tipDetails is not False:
                    if tipDetails != "Trade":
                        tipDetailsList.append(tipDetails)

                new_tips_urls.append(url)
            for tip in tipDetailsList:
                compileAndSendTip(tip)
        self.state['blogabet_tips'] = new_tips_urls

        self.saveState()
        return True
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            ## getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\","\\\\")
            err = traceback.format_exc().replace("\\","\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Getting Greyhound Results",
                            machine: "{ip_address}",
                            machineName: "API",
                            errorFileName: "{file}",
                            err: "{err},
                            critical: true
                        }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        traceback.print_exc()
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Tip From Blogabet",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
